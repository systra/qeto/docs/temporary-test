module.exports = {
    title: 'Vizeo - Documentation utilisateurs',
    description: 'Documentation pour les utilisateurs de Vizeo',
    base: '/qeto/docs/temporary-test/',
    dest: 'public',
    themeConfig: {
        sidebar: [
          ['/', 'Accueil'],
          {
            title: 'Contribuer au projet',
            sidebarDepth: 0,
            collapsable: false,
            children: [
              '/contribuer-au-projet/uploader-une-version-de-document.md',
              '/contribuer-au-projet/visualiser-les-documents.md',
            ]
          },
          {
            title: 'Gérer un workflow',
            sidebarDepth: 0,
            collapsable: false,
            children: [
              '/gerer-un-workflow/introduction.md',
              '/gerer-un-workflow/etape-recevabilite.md',
            ]
          },
          {
            title: 'Paramétrer un projet',
            sidebarDepth: 0,
            collapsable: false,
            children: [
              '/parametrer-un-projet/creer-un-projet.md',
              '/parametrer-un-projet/parametrer-nom-sharepiont-marches.md',
              '/parametrer-un-projet/parametrer-metadonnes-codification-workflows.md',
              '/parametrer-un-projet/parametrer-contributions.md',
            ]
          },
          {
            title: 'FAQ',
            sidebarDepth: 0,
            collapsable: false,
            children: [
              '/faq/faq.md',
            ]
          },
        ]
      }
}
